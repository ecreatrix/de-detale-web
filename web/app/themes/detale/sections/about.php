<div class="section-about">
  <div class="section-content">
    <div class="animated container container-sm">
      <!-- <div class="section-title">
     <div class="title-and-corner">
          <div class="title-corner">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
      <polygon class="st0" points="500,500 0,500 0,0 "/>
      </svg>
    </div>
    <h1><?php the_title();?></h1>
          </div>
  </div> -->
      <?php the_content();?>
    </div>

  </div>
</div>
    <span id="splash-arrow-black-about" class="splash-arrow"><svg>
  <polyline class="st0" points="21.6,14.8 16.8,19.5 12.1,14.8 "></polyline>
    </svg></span>
<div class="border-container">
<span class="border-corner"></span>
</div>
