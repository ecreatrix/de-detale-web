<div class="section-contact">
  <div class="section-content">
    <div class="container container-sm">
      <!-- <div class="section-title">
	   <div class="title-and-corner">
      		<div class="title-corner">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
			<polygon class="st0" points="500,500 0,500 0,0 "/>
			</svg>
		</div>
		<h1><?php the_title(); ?></h1>
          </div> -->
	</div>
      <div id="company-contact">
      		<?php for ($i=1; $i < 5; $i++) { ?>
			<div class="contact-<?php echo $i; ?>">
			<?php if (get_field('contact_title_'.$i)){ ?>
				<div id="title-<?php echo $i?>" class="contact-info-title">
					<h3 class="contact-link"><?php the_field('contact_title_'.$i); ?></h3>
				</div>
				<div class="contact-info-content contact-content-<?php echo $i?>">
				 	<p><?php the_field('contact_information_'.$i); ?></p>
				</div>
			<?php } ?>
			</div>
		<?php } ?>
      </div>
    </div>
  </div>
</div>