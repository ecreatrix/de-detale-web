<?php
/**
 * Detale functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Detale
 */

if (!function_exists('detale_setup')):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
    function detale_setup()
{
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Detale, use a find and replace
         * to change 'detale' to the name of your theme in all the template files.
         */
        load_theme_textdomain('detale', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus([
            'primary' => esc_html__('Primary', 'detale'),
        ]);

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ]);

        /*
         * Enable support for Post Formats.
         * See https://developer.wordpress.org/themes/functionality/post-formats/
         */
        add_theme_support('post-formats', [
            'aside',
            'image',
            'video',
            'quote',
            'link',
        ]);

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('detale_custom_background_args', [
            'default-color' => 'ffffff',
            'default-image' => '',
        ]));
    }
endif; // detale_setup
add_action('after_setup_theme', 'detale_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function detale_content_width()
{
    $GLOBALS['content_width'] = apply_filters('detale_content_width', 640);
}
add_action('after_setup_theme', 'detale_content_width', 0);

/*------------------------------------*\
Register widget area.
\*------------------------------------*/
/**
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function detale_widgets_init()
{
    register_sidebar([
        'name'          => esc_html__('Sidebar', 'detale'),
        'id'            => 'sidebar-1',
        'description'   => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ]);
}
add_action('widgets_init', 'detale_widgets_init');

function get_file_time($filename)
{
    $path = get_template_directory();
    $uri  = get_template_directory_uri();

    // Get file reload. Plugin/Theme version for external scripts and file edit time for internal scripts
    $reload = '0.5';

    if (file_exists($path . $filename)) {
        $reload = filemtime($path . $filename);
    }

    return $reload;
}
/**
 * Enqueue scripts and styles.
 */
function detale_scripts()
{
    wp_enqueue_script('detale-slidereveal', get_template_directory_uri() . '/js/jquery.slidereveal.min.js', ['jquery', 'jquery-effects-core'], get_file_time('js/jquery.slidereveal.min.js'), true);

    wp_enqueue_script('detale-sectionsnap', get_template_directory_uri() . '/js/jquery-sectionsnap.js', ['jquery', 'jquery-effects-core'], get_file_time('js/jquery-sectionsnap.js'), true);

    wp_enqueue_script('detale-jail', get_template_directory_uri() . '/js/jail.js', ['jquery', 'jquery-effects-core'], get_file_time('js/jail.js'), true);

    wp_enqueue_script('detale-cycle', get_template_directory_uri() . '/js/jquery.cycle.all.js', ['jquery', 'jquery-effects-core'], get_file_time('js/jquery.cycle.all.js'), true);

    wp_enqueue_script('detale-slick', get_template_directory_uri() . '/slick/slick.min.js', ['jquery', 'jquery-effects-core'], get_file_time('slick/slick.min.js'), true);

    wp_enqueue_script('detale-navigation', get_template_directory_uri() . '/js/navigation.js', [], get_file_time('/js/navigation.js'), true);

    wp_enqueue_script('detale-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', [], get_file_time('/js/skip-link-focus-fix.js'), true);

    wp_enqueue_script('detale-scripts', get_template_directory_uri() . '/js/scripts.js', ['jquery', 'detale-cycle', 'detale-slick', 'detale-slidereveal', 'detale-sectionsnap', 'detale-navigation', 'detale-skip-link-focus-fix'], get_file_time('/js/scripts.js'), true);
}
add_action('wp_enqueue_scripts', 'detale_scripts');
function detale_styles()
{
    wp_enqueue_style('detale-style', get_stylesheet_uri(), [], get_file_time('/style.css'));
    wp_enqueue_style('detale-slick', get_template_directory_uri() . '/slick/slick.css', [], get_file_time('/slick/slick.css'));
    wp_enqueue_style('detale-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|Open+Sans:400,300,600,700,800');
    wp_enqueue_style('detale-fullpage', get_template_directory_uri() . '/fullPage/jquery.fullPage.css', [], get_file_time('/fullPage/jquery.fullPage.css'));
}

add_action('wp_enqueue_scripts', 'detale_styles');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

add_action('init', 'create_post_type_portfolio');
add_action('init', 'create_post_type_slides');

function create_post_type_portfolio()
{
    register_post_type('portfolio', // Register Custom Post Type
        [
            'labels'       => [
                'name'               => __('portfolio', 'detale'), // Rename these to suit
                'singular_name'      => __('portfolio_item', 'detale'),
                'add_new'            => __('Add New', 'detale'),
                'add_new_item'       => __('Add New Portfolio Items', 'detale'),
                'edit'               => __('Edit', 'detale'),
                'edit_item'          => __('Edit Portfolio Items', 'detale'),
                'new_item'           => __('New Portfolio Items', 'detale'),
                'view'               => __('View Portfolio Items', 'detale'),
                'view_item'          => __('View Portfolio Item', 'detale'),
                'search_items'       => __('Search Portfolio Items', 'detale'),
                'not_found'          => __('No Portfolio items found', 'detale'),
                'not_found_in_trash' => __('No Portfolio items found in Trash', 'detale'),
            ],
            'public'       => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive'  => false,
            'supports'     => [
                'title',
            ], // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export'   => true, // Allows export in Tools > Export
            'taxonomies'   => [],
        ]);
}
function create_post_type_slides()
{
    register_post_type('slides', // Register Custom Post Type
        [
            'labels'       => [
                'name'               => __('slides', 'detale'), // Rename these to suit
                'singular_name'      => __('slide', 'detale'),
                'add_new'            => __('Add New', 'detale'),
                'add_new_item'       => __('Add New Slide Items', 'detale'),
                'edit'               => __('Edit', 'detale'),
                'edit_item'          => __('Edit Slide', 'detale'),
                'new_item'           => __('New Slide', 'detale'),
                'view'               => __('View Slide', 'detale'),
                'view_item'          => __('View Slide', 'detale'),
                'search_items'       => __('Search Slides', 'detale'),
                'not_found'          => __('No Slides found', 'detale'),
                'not_found_in_trash' => __('No Slides found in Trash', 'detale'),
            ],
            'public'       => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive'  => false,
            'supports'     => [
                'title',
            ], // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export'   => true, // Allows export in Tools > Export
            'taxonomies'   => [],
        ]);
}
