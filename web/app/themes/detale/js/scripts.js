$ = jQuery.noConflict();

$( document ).ready( function() {


  ////////////////////////////        
  // Define site variables 
  ////////////////////////////

  var browser_width = $( window ).width();
  var portfolio_items = [];

  ////////////////////////////        
  // Retrive Portfolio Items
  ////////////////////////////

  $( ".portfolio-item" ).each( function( index, element ) {
    var port_name = $( this ).attr( "class" );
    port_name = port_name.replace( "portfolio-item", "" ).split( "-" );
    class_name = port_name[ 2 ];
    class_name = class_name.split( " " );
    portfolio_items.push( class_name[ 0 ] );
  } );


  //////////////////////////////////
  // Setup of each portfolio item
  //////////////////////////////////

  $( function( $ ) {

    $.each( portfolio_items, function( index, value ) {
      eval( "slider_" + index + " = value" );
    } );

    for ( var i = 0, l = portfolio_items.length; i < l; i++ ) {

      window[ "slider-" + i ] = $( ".portfolio-" + portfolio_items[ i ] ).slideReveal( {
        trigger: $( ".portfolio-overlay-" + portfolio_items[ i ] ),
        speed: 400,
        width: browser_width,
        push: false,
        overlay: true,
        show: function( slider ) {
          $( 'body' ).css( {
            "overflow": "hidden"
          } );
          $( slider ).removeClass( "hidden-content" );
          $( '.portfolio-exit-bg' ).css( "position", "fixed" );
          $( '.portfolio-area-exit' ).css( "position", "fixed" );
          mobileOverlayScroll();
        },
        hide: function( slider, trigger ) {
          $( 'body' ).css( {
            "overflow": "scroll",
            "position": "initial"
          } );
          $( slider ).addClass( "hidden-content" );
          $( '.portfolio-exit-bg' ).css( "position", "absolute" );
          $( '.portfolio-area-exit' ).css( "position", "absolute" );
          mobileScroll();
        }
      } );

    }

  } );

  var mobileOverlayScroll = function() {
    if ( navigator.userAgent.match( /(iPhone|iPod|iPad)/i ) ) {
      setTimeout( function() {
        $( 'body' ).css( {
          "position": "fixed"
        } );
      }, 500 );

      $currentScrollPos = $( document ).scrollTop();
      localStorage.cachedScrollPos = $currentScrollPos;
    }
  };
  var mobileScroll = function() {
    if ( navigator.userAgent.match( /(iPhone|iPod|iPad)/i ) ) {
      $( 'body' ).scrollTop( localStorage.cachedScrollPos );
    }
  };
  //////////////////////////////////////////////////////////////        
  // For each portfolio item, exit on click of exit icon
  //////////////////////////////////////////////////////////////

  $( ".portfolio-area-exit" ).click( function() {
    $.each( portfolio_items, function( index, value ) {
      $( '.portfolio-' + value ).slideReveal( "hide" );
    } );

  } );
  $( ".portfolio-exit-bg" ).click( function() {
    $.each( portfolio_items, function( index, value ) {
      $( '.portfolio-' + value ).slideReveal( "hide" );
    } );

  } );
  $( ".portfolio-back" ).click( function() {
    $.each( portfolio_items, function( index, value ) {
      $( '.portfolio-' + value ).slideReveal( "hide" );
    } );

  } );

  ///////////////////////////////////    
  // Color Change Function
  ///////////////////////////////////

  var changeColor = setInterval( function() {
    var colors = [ '#da4032', '#f1b13b', '#1a4687', '#00a67e' ];
    var color = colors[ Math.floor( Math.random() * colors.length ) ];
    $( "#splashscreen-corner" ).css( {
      fill: color
    } );
    $( ".splashscreen-slide" ).css( {
      background: color
    } )
  }, 3000 );


  ///////////////////////////////////    
  // Portfolio item click
  ///////////////////////////////////

  $( "#portfolio-trigger" ).hover( function() {
    $( this ).find( ".portfolio-tags" ).animate( {}, 1000 )
  } );

  ///////////////////////////////////    
  //Browser resizing animation
  ///////////////////////////////////    

  //Initial size of Portfolio Banner

  $( window ).resize( function() {
    //Portfolio Banner resize animation 
    var slider_width = $( '#portfolio-content .portfolio-slider img' ).width();
    var slider_height = slider_width / 1.6;
    $( '#portfolio-content .portfolio-slider img' ).css( {
      'height': slider_height + 'px'
    } );
  } );


  ////////////////////////////
  // Site animations
  ////////////////////////////

  $( "#splash-arrow-black" ).click( function() {
    $( 'html,body' ).animate( {
      scrollTop: $( '#about-detale' ).offset().top
    }, 1200 );
  } );
  $( "#splash-arrow-black-about" ).click( function() {
    $( 'html,body' ).animate( {
      scrollTop: $( '#portfolio' ).offset().top
    }, 1200 );
  } );

  ////////////////////////////////////////////
  // Splashscreen animation functions
  ////////////////////////////////////////////
  $( ".home #splashscreen-copy" ).animate( {
    opacity: 1
  }, 500, function() {} );
  $( ".home #animated-slides" ).delay( 1500 ).animate( {
    opacity: 1
  }, 2000 );


  $( '#animated-slides' ).slick( {
    prevArrow: '<div id="left-button" class="slick-prev"></div>',
    nextArrow: '<div id="right-button" class="slick-next"></div>',
    autoplay: true,
    fade: true
  } );

  $( '.animated-logo-area' ).cycle( {
    delay: 2000,
    fx: 'scrollVert',
    speed: 400,
    timeout: 300,
    autostop: 1,
    autostopCount: $( '.animated-logo-area > img' ).length + 1,
    end: function() {}
  } );


  //////////////////////////////////////
  // Portfolio Icon Cycle
  //////////////////////////////////////


  $( '.portfolio-slider' ).slick( {
    prevArrow: '<div id="left-button" class="port-slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M229.9 473.899l19.799-19.799c4.686-4.686 4.686-12.284 0-16.971L94.569 282H436c6.627 0 12-5.373 12-12v-28c0-6.627-5.373-12-12-12H94.569l155.13-155.13c4.686-4.686 4.686-12.284 0-16.971L229.9 38.101c-4.686-4.686-12.284-4.686-16.971 0L3.515 247.515c-4.686 4.686-4.686 12.284 0 16.971L212.929 473.9c4.686 4.686 12.284 4.686 16.971-.001z"/></svg></div>',
    nextArrow: '<div id="right-button" class="port-slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M218.101 38.101L198.302 57.9c-4.686 4.686-4.686 12.284 0 16.971L353.432 230H12c-6.627 0-12 5.373-12 12v28c0 6.627 5.373 12 12 12h341.432l-155.13 155.13c-4.686 4.686-4.686 12.284 0 16.971l19.799 19.799c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L235.071 38.101c-4.686-4.687-12.284-4.687-16.97 0z"/></svg></div>',
    fade: true,
    //dots: true
  } );



  //$( ".icon-table").click(function() {
  //       if($( ".icon-table").css("height") == "205px"){
  //       $( ".icon-table").animate({
  //                 height: '550px',
  //           }, 1000 , function(){$( ".icon-table").css( "cursor", "n-resize" ); }  
  //       ); } else {
  //       $( ".icon-table").animate({
  //                 height: '205px'
  //           }, 1000, function(){$( ".icon-table").css( "cursor", "s-resize" ); }  
  //       ); }
  //                       
  //});

  //////////////////////////////////////
  // Contact area reveal
  //////////////////////////////////////

  $( ".contact-info-title" ).click( function() {
    id = this.id.replace( "title-", "" );

    $( ".contact-info-content" ).slideUp( 500 );
    $( ".contact-content-" + id ).slideDown( 500, function() {
      $( ".contact-content-1" ).addClass( "open" );
    } );
  } );


  ////////////////////////////////////////////
  // Splashscreen copy hover and click
  //////////////////////////////////////////// 


  $( '#splashscreen-copy' ).hover( function() {

    $( "#splashscreen .base-logo" ).css( {
      'transition-duration': '0s',
      'opacity': '0'
    } )
  }, function() {
    $( "#splashscreen .base-logo" ).css( {
      'transition-duration': '0s',
      'opacity': '1'
    } );


    $( "#splashscreen .base-logo" ).css( {
      'transition-duration': '0s',
      'opacity': '1'
    } )
  }, function() {
    $( "#splashscreen .base-logo" ).css( {
      'transition-duration': '0s',
      'opacity': '1'
    } );
  } );
  $( '#splashscreen-copy' ).hover( function() {

    $( "#splashscreen .hover-logo" ).css( {
      'transition-duration': '0s',
      'opacity': '1'
    } )
  }, function() {
    $( "#splashscreen .hover-logo" ).css( {
      'transition-duration': '0s',
      'opacity': '0'
    } );


    $( "#splashscreen .hover-logo" ).css( {
      'transition-duration': '0s',
      'opacity': '0'
    } )
  }, function() {
    $( "#splashscreen .hover-logo" ).css( {
      'transition-duration': '0s',
      'opacity': '0'
    } );
  } );

  $( '#splashscreen-copy' ).click( function() {
    ////hide_splashscreen();
    $( 'html,body' ).animate( {
      scrollTop: $( '#about-detale' ).offset().top
    }, 1200 );
  } );

  $( '#detale-corner' ).click( function() {
    hide_splashscreen();

  } );

  ////////////////////////////////////////////
  // Splashscreen animation functions
  //////////////////////////////////////////// 

  var hide_splashscreen = function() {
    $( "#splashscreen-container" ).animate( {
      opacity: '0'
    }, 2500, function() {
      $( "#splash-arrow-white" ).fadeOut( 600 );
      $( "#splashscreen-content" ).fadeIn( 1000 );
      $( "#splash-arrow-black" ).delay( 1800 ).fadeIn( 1000 );
      $( ".splashscreen-slide" ).css( 'display', 'none' );
    } );
    $( "#splashscreen-copy" ).fadeOut( 1000 );
    $( "#animated-slides" ).animate( {
      opacity: '0'
    }, 2000, function() {
      $( "#animated-slides" ).css( 'display', 'none' )
    } );
  };

  ////////////////////////////
  // Bounce Animations 
  ////////////////////////////
  setInterval(function() {
      $('.splash-arrow svg').effect('bounce', {
          distance: 7.5
          }, 1500)
  }, 2000);

  ////////////////////////////
  // Menu Animations 
  ////////////////////////////

  function goToByScroll( id ) {
    // Remove "link" from the ID
    id = id.replace( "http://detale.ca/#", "" );
    id = id.replace( "https://detale.ca/#", "" );

    //console.log(id);
    // Scroll
    $( 'html,body' ).animate( {
        scrollTop: $( "#" + id ).offset().top
      },
      800,
      function() {} );
    setTimeout( function() {
      hide_show_menu()
    }, 900 );
  }

  $( ".menu-main-nav-container > ul > li > a" ).click( function( e ) {
    // Prevent a page reload when a link is pressed
    e.preventDefault();
    // Call the scroll function
    goToByScroll( this.href );

  } );
  ////////////////////////////
  // Scroll to Portfolio
  ////////////////////////////
  $( ".slide-information" ).click( function( e ) {
    // Prevent a page reload when a link is pressed
    e.preventDefault();
    // Scroll to Portfolio
    $( 'html,body' ).animate( {
        scrollTop: $( "#portfolio" ).offset().top
      },
      800 );
  } );


  ////////////////////////////
  // Open Close Main Menu
  ////////////////////////////
  $( '#burger-menu' ).click( function() {
    hide_show_menu();
  } );

  var hide_show_menu = function() {
    if ( $( "#masthead" ).hasClass( "closed" ) ) {
      $( '#masthead' )
        .stop( true, true )
        .animate( {
          height: "toggle",
          opacity: "toggle"
        }, 0 );

      $( '.stick' )
        .animate( {
          top: "10px"
        }, 300, function() {
          $( "#stick2" ).css( {
            'display': 'none'
          } );
          $( "#stick1" ).css( {
            'transition-duration': '0.5s',
            '-webkit-transform': 'rotate(45deg)',
            '-moz-transform': 'rotate(45deg)',
            '-ms-transform': 'rotate(45deg)',
            'transform': 'rotate(45deg)'
          } );
          $( "#stick3" ).css( {
            'transition-duration': '0.5s',
            '-webkit-transform': 'rotate(-45deg)',
            '-moz-transform': 'rotate(-45deg)',
            '-ms-transform': 'rotate(-45deg)',
            'transform': 'rotate(-45deg)'
          } );
        } );

      $( "#masthead" ).removeClass( "closed" )
    } else {
      $( '#masthead' )
        .stop( true, true )
        .animate( {
          height: "toggle",
          opacity: "toggle"
        }, 0 );
      $( '.stick' )
        .animate( {
          top: "inherit"
        }, 300, function() {
          $( "#stick1" ).css( {
            'transition-duration': '0.5s',
            '-webkit-transform': 'rotate(0deg)',
            '-moz-transform': 'rotate(0deg)',
            '-ms-transform': 'rotate(0deg)',
            'transform': 'rotate(0deg)',
            'top': '4px'
          } );
          $( "#stick3" ).css( {
            'transition-duration': '0.5s',
            '-webkit-transform': 'rotate(0deg)',
            '-moz-transform': 'rotate(0deg)',
            '-ms-transform': 'rotate(0deg)',
            'transform': 'rotate(0deg)',
            'top': '16px'
          } );
          $( "#stick2" ).css( {
            'transition-duration': '0.5s',
            'display': 'block'
          } );
        } );
      $( "#masthead" ).addClass( "closed" )
    }

  };
  ////////////////////////////////
  // About Section Animation
  ////////////////////////////////       


  $( window ).scroll( function() {
    var y = $( window ).scrollTop(),
      x = $( '#about-detale' ).offset().top - 255;
    //z = $('.animated').offset().top + $('.animated').height();
    if ( y > x ) {
      $( '.animated' ).delay( 2000 ).addClass( 'fadeInUp' ).removeClass( 'fadeOutDown' );
      $( "#splash-arrow-black-about" ).delay( 1800 ).fadeIn( 1000 );

    } else {
      $( "#splash-arrow-black-about" ).fadeOut( 1000 );
    }
  } );


  ////////////////////////////
  // Snap to section 
  ////////////////////////////

  var isMobile = {
    Android: function() {
      return navigator.userAgent.match( /Android/i );
    },
    BlackBerry: function() {
      return navigator.userAgent.match( /BlackBerry/i );
    },
    iOS: function() {
      return navigator.userAgent.match( /iPhone|iPad|iPod/i );
    },
    Opera: function() {
      return navigator.userAgent.match( /Opera Mini/i );
    },
    Windows: function() {
      return navigator.userAgent.match( /IEMobile/i );
    },
    any: function() {
      return ( isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() );
    }
  };
  if ( !isMobile.any() ) {
    $( "#main" ).sectionsnap( {
      delay: 300,
      animationTime: 600,
      selector: '.section'
    } );
  }

  ////////////////////////////
  // Snap to section 
  ////////////////////////////
  var back = [ '#da4032', '#f1b13b', '#1a4687', '#00a67e' ];
  var menu_color = back[ Math.floor( Math.random() * back.length ) ];
  $( window ).bind( 'scroll', function() {
    var scroll_pos = $( window ).scrollTop();
    var about_pos_start = $( "#about-detale" ).position().top - 35;
    var about_pos_end = $( "#about-detale" ).height() + $( "#about-detale" ).position().top;
    var contact_pos_start = $( "#contact" ).position().top - 35;
    var contact_pos_end = $( "#contact" ).height() + $( "#contact" ).position().top;

    if ( ( about_pos_start < scroll_pos && scroll_pos < about_pos_end ) || ( contact_pos_start < scroll_pos && scroll_pos < contact_pos_end ) ) {
      $( ".stick" ).css( {
        "background-color": menu_color
      } );
      $( "#burger-menu span" ).css( {
        "color": menu_color
      } );
    } else {
      $( ".stick" ).css( {
        "background-color": "#ddd"
      } );
      $( "#burger-menu span" ).css( {
        "color": "#ddd"
      } );
    }
  } );

} );
$( function() {
  var portfolio_items = [];
  $( ".portfolio-item" ).each( function( index ) {
    var port_trigger = $( this ).attr( 'class' ).split( " " )[ 1 ];
    var port_name = port_trigger.split( "-" )[ 2 ]
    var port_identifier = '.portfolio-' + port_name + ' .portfolio-images .lazy-load';
    $( port_identifier ).jail( {
      triggerElement: '.portfolio-overlay-' + port_name,
      event: 'click',
      effect: 'fadeIn',
      speed: 200,
      placeholder: '/app/themes/detale/img/placeholder.png'
    } );

  } );
} );
