<?php
/**
 * The header for Detale theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Detale
 */

?><!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
<title>Detale Creative Agency, Toronto</title>
<meta name="keywords" content="Detale,Detale Toronto,Detale Canada,Detale Creative agency, Detale Agency, Creative agency Toronto">
<meta charset="<?php bloginfo('charset');?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php echo get_field('meta_description', 14) ?>">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

<?php wp_head();?>
</head>
<body <?php body_class();?>>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'detale');?></a>

	<div id="burger-menu" class="burger-menu sidebar">
			<span>MENU</span>
			<div class="sticks_wrapper">
				<div class="stick -first" id="stick1"></div>
				<div class="stick -second" id="stick2"></div>
				<div class="stick -third" id="stick3"></div>
			</div>
	</div>

	<header id="masthead" class="site-header closed" role="banner">
		<div class="site-branding">
			<?php
if (is_front_page() && is_home()): ?>
				<h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name');?></a></h1>
				<div id="site-logo"></div>
			<?php else: ?>
				<div id="site-logo"></div>
			<?php
endif;
$description = get_bloginfo('description', 'display');
if ($description || is_customize_preview()): ?>
			<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>

			<?php
endif;?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu(['theme_location' => 'primary', 'menu_id' => 'primary-menu']);?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">