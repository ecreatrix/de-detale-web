<?php
	/**
	 * The template for displaying the footer.
	 *
	 * Contains the closing of the #content div and all content after.
	 *
	 * @package Detale
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			All rights reserved Detale			                          			                           <?php echo date( 'Y' ); ?>.
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer();?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/slick/slick.min.js"></script>

</body>
</html>
