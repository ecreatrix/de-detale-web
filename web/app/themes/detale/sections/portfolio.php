<div class="section-portfolio">
  <div class="section-content">
    <div class="container container-sm">
	<!-- <div class="section-title">
	   <div class="title-and-corner">
      		<div class="title-corner">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
			<polygon class="st0" points="500,500 0,500 0,0 "/>
			</svg>
		</div>
		<h1><?php the_title();?></h1>
          </div>
	</div> -->
	<div class="portfolio-wrapper">

		<?php
query_posts( ['post_type' => 'portfolio'] );
$count = 1;
if ( have_posts() ): while ( have_posts() ): the_post();
		?>

																				<div id="portfolio-trigger" class="portfolio-item portfolio-overlay-<?php echo the_title(); ?>" > <img src="<?php $image_item = 'portfolio_link_image';
		$image                                                                                                                                          = get_field( $image_item );
		echo $image['url'];?>" alt="Portfolio<?php echo the_title(); ?>"><div class="link-wrapper"><h6><?php echo the_title() ?></h6><p class="portfolio-tags"><?php echo the_field( 'portfolio_tags' ); ?></p></div><div class=" port-color-<?php echo $count;
		$link_color_no = $count;
		$count++;if ( $count > 8 ) {$count = 1;} ?> port-image-overlay"></div>
																				<div class= "portfolio-corner">
																					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
																						<polygon class="st0" points="500,500 0,500 0,0 "/>
																					</svg>
																				</div>
																				</div>

																				<?php endwhile;endif;
wp_reset_query();?>
	</div>
	<div id="portfolio-container">

<?php
query_posts( ['post_type' => 'portfolio'] );
$placeholder = get_template_directory_uri() . '/img/placeholder.png';

if ( have_posts() ): while ( have_posts() ): the_post();
		?>

																				<div id="portfolio-content" class="hidden-content portfolio-<?php echo the_title(); ?> single-portfolio">

																					<span class="portfolio-area-exit"></span>
																					<span class="portfolio-exit-bg"></span>

																		  			<div class='portfolio-information'>
																						<div class='portfolio-information-inner port-color-<?php echo $link_color_no;
		$link_color_no++;if ( $link_color_no > 8 ) {$link_color_no = 1;} ?>'>
																							<span class="portfolio-back">Back</span>
																							<div class="portfolio-title"><h3><?php the_title();?></h3></div>
																							<div class="portfolio-desc"><?php the_field( 'description' );?></div>
																							<div class="portfolio-services">
																								<p class='serv-title'>Services</p>
																								<p><?php the_field( 'services' );?></p>
																							</div>
																							<?php if ( get_field( 'btn_1' ) || get_field( 'btn_2' ) ) {?>
																								<div class="portfolio-btns">
																									<?php if ( get_field( 'website_url' ) ) {?>
																									<a class="portfolio_btn site-btn" target="_blank" href="<?php echo get_field( 'website_url' ) ?>">Launch Site</a>
																									<?php }if ( get_field( 'brand_url' ) ) {?>
																									<a class="portfolio_btn brand-btn" target="_blank" href="<?php echo get_field( 'brand_url' ) ?>">View Brand Guide</a>
																									<?php }?>
																								</div>
																							<?php }?>
																						</div>
																					</div>
																					<div class="portfolio-images">
																					<?php if ( get_field( 'portfolio_first_image' ) ) {?>
																						<img src="<?php echo get_field( 'portfolio_first_image' ); ?>" alt="First Image" />
																					<?php }?>
						<?php if ( get_field( 'portfolio_second_image' ) ) {?>
																						<img class="lazy-load" src="<?php echo $placeholder; ?>" data-src="<?php echo get_field( 'portfolio_second_image' ); ?>" alt="Second Image" />
																					<?php }?>
						<?php if ( get_field( 'portfolio_third_image' ) ) {?>
																						<img class="lazy-load" src="<?php echo $placeholder; ?>" data-src="<?php echo get_field( 'portfolio_third_image' ); ?>" alt="Third Image" />
																					<?php }?>
						<?php if ( get_field( 'custom_content' ) ) {?>
																						<div class="portfolio-custom-content"><?php the_field( 'custom_portfolio_content' )?></div>
																					<?php }?>
						<?php
		for ( $i = 1; $i < 8; $i++ ) {
			?>
						<?php
		$image_item = 'portfolio_image_' . $i;
			$image      = get_field( $image_item );
			if ( ! empty( $image ) ): ?>
																							<img class="lazy-load" src="<?php echo $placeholder; ?>" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
																							<?php endif;?>
			<?php }?>
			<?php if ( get_field( 'slider_image_1' ) ) {?>
													<div class="portfolio-slider test">
														<div class="img-container"><img src="<?php the_field( 'slider_image_1' )?>" alt="Slider Image"/></div>
														<?php if ( get_field( 'slider_image_2' ) ) {?>
															<div class="img-container"><img class="lazy-load" src="<?php echo $placeholder; ?>" data-src="<?php the_field( 'slider_image_2' )?>" alt="Slider Image"/></div>
														<?php }?>
			<?php if ( get_field( 'slider_image_3' ) ) {?>
															<div class="img-container"><img class="lazy-load" src="<?php echo $placeholder; ?>" data-src="<?php the_field( 'slider_image_3' )?>" alt="Slider Image"/></div>
														<?php }?>
			<?php if ( get_field( 'slider_image_4' ) ) {?>
															<div class="img-container"><img class="lazy-load" src="<?php echo $placeholder; ?>" data-src="<?php the_field( 'slider_image_4' )?>" alt="Slider Image"/></div>
														<?php }?>
			<?php if ( get_field( 'slider_image_5' ) ) {?>
															<div class="img-container"><img class="lazy-load" src="<?php echo $placeholder; ?>" data-src="<?php the_field( 'slider_image_5' )?>" alt="Slider Image"/> </div>
														<?php }?>
			<?php if ( get_field( 'slider_image_6' ) ) {?>
															<div class="img-container"><img class="lazy-load" src="<?php echo $placeholder; ?>" data-src="<?php the_field( 'slider_image_6' )?>" alt="Slider Image"/></div>
														<?php }?>




													</div>
												<?php }?>

												</div>
									                      </div>
											</div>
											<?php endwhile;endif;
wp_reset_query();?>
	</div>
</div>
</div>