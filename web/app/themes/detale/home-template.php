<?php /* Template Name: Homepage */?>
<?php get_header();?>

<div id="primary" class="content-area">
	<main id="main" class="site-main fullPage" role="main">
	<section id="splashscreen" class="section" data-anchor="home">

	<div id="animated-slides">
	<?php query_posts( ['post_type' => 'slides'] );
		if ( have_posts() ): while ( have_posts() ): the_post();
			?>

								<div id="slide-<?php echo the_field( 'slide_num' ); ?>" class="single-slide">
									<div class="slide-image">
									<?php
												$image_item = 'slide_image';
												$image = get_field( $image_item );
											if ( ! empty( $image ) ) {?>
											<div class="slide-image-container slide-image-<?php echo the_field( 'slide_ratio' ); ?>" style="background-image:url(<?php echo $image['url']; ?>)"></div>
											<?php }?>
									</div>
									<a class='slide-information' href="#portfolio"><?php the_field( 'slide_desc' );?></a>
								</div>
							<?php endwhile;endif;
							wp_reset_query();?>
	</div>
		<div id="splashscreen-container">
		<div id="splashscreen-slide-bg" class="splashscreen-slide"></div>
		</div>

		<div id="splashscreen-copy">
			<img class="splash-logo-mobile" src="<?php echo get_template_directory_uri(); ?>/img/detale-upd-mobile.svg" alt="Detale logo">

		<div id="animated-logo">
		  <img class="base-logo"  src="<?php echo get_template_directory_uri(); ?>/img/animated/hover-logo.svg" alt="Detale base logo">
		  <img class="hover-logo"  src="<?php echo get_template_directory_uri(); ?>/img/animated/hover-logo.svg" alt="Detale logo">
		<div class="animated-logo-area">

 		   <img class="anm-logo-main"  src="<?php echo get_template_directory_uri(); ?>/img/animated/png/animate-detale.png" alt="Detale">


		 </div>


			</div>




		</div>
		<div id="splashscreen-content">
			<svg id="splashscreen-corner"><polygon class="splash-corner" points="500,500 0,500 0,0 "/></svg>
			<h1 class="underlined-title"><?php the_field( 'tag_line' );?></h1>
			<h2><?php the_field( 'tag_line_descriptor' );?></h2>
			<span id="splash-arrow-black" class="splash-arrow"><svg>
				<polyline class="st0" points="21.6,14.8 16.8,19.5 12.1,14.8 "/>
			</svg></span>
		</div>

		<span id="splash-arrow-white" class="splash-arrow"><svg>
			<polyline class="st0" points="21.6,14.8 16.8,19.5 12.1,14.8 "/>
		</svg></span>
	</section>

<?php if ( have_posts() ): while ( have_posts() ): the_post();
			$sections = [];
			for ( $i = 1; $i < 10; $i++ ) {
				if ( get_field( 'section_' . $i ) ) {
					$temp = get_field( 'section_' . $i );
					if ( isset( $temp->ID ) ) {
						$template = str_replace( '.php', '', str_replace( 'template-', '', get_page_template_slug( $temp->ID ) ) );
						if ( $template && 'home' != $template ) {
							$sections[] = [
								'id'       => $temp->ID,
								'title'    => $temp->post_title,
								'name'     => $temp->post_name,
								'template' => $template
							];
						}
					}
				}
		}?>

						<!-- Display all content areas -->

						<?php
									foreach ( $sections as $section ) {
										$page_sections = new WP_Query( ['page_id' => $section['id']] );
									if ( $page_sections->have_posts() ) {while ( $page_sections->have_posts() ) {$page_sections->the_post();?>
						      <section id="<?php echo $section['name'] ?>" class="<?php echo $section['template']; ?> page_section section" data-anchor="">
						        <?php include 'sections/' . $section['template'] . '.php';?>
						      </section>
						    <?php
						    			}}
						    				wp_reset_postdata();
						    			}
						    		?>



							</main><!-- #main -->



							</div><!-- #primary -->

						<?php endwhile;endif;?>
<span id="detale-corner"></span>
<?php get_footer();?>